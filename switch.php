<!DOCTYPE html>
<html>
<head>
	<title>switchphp</title>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html">
</head>
<body>
<form action="" method="POST">
	<p>Selecteer een land</p>
	<select name="land" value="true">
		<option value=""></option>
		<option value="nl">Nederland</option>
		<option value="be">Belgie</option>
		<option value="de">Duitsland</option>
		<option value="se">Spanje</option>
	</select>
	<input type="submit" name="submit" value="Versturen">
	<br><br><br><br><br><br><br><br>
	<p>----------------------------------------------</p>
</form>
<?php
	if (isset($_POST['submit'])) {
		switch ($_POST['land']) {
			case 'nl':
				echo "U heeft Nederland gekozen";
				break;
			case 'be':
				echo "U heeft Belgie gekozen";
				break;
			case 'de':
				echo "U heeft Duitsland gekozen";
				break;
			case 'se':
				echo "U heeft Spanje gekozen";
				break;

			default:
				echo "U heeft geen land gekozen";
				break;
		}
	}


?>
</body>
</html>